# Sofa Position Based Dynamics Plugin

## Cloning and Compiling
First, make sure you have compiled and install sofa. Note the sofa's installation directory path (ex. /home/user/sofa/build/install).

```sh
git clone https://gitlab.com/jnbrunet/SofaPositionBasedDynamicsPlugin.git
cd SofaPositionBasedDynamicsPlugin
mkdir build
cd build
cmake -DCMAKE_PEFIX_PATH=$SOFA_PATH/lib/cmake ..
```
Don't forget to change `$SOFA_PATH` to your sofa's installation directory path.
