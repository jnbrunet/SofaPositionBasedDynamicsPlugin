/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, development version     *
*                (c) 2006-2017 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This program is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.        *
*******************************************************************************
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_COMPONENT_ANIMATIONLOOP_PBDANIMATIONLOOP_H
#define SOFA_COMPONENT_ANIMATIONLOOP_PBDANIMATIONLOOP_H
#include <sofa/simulation/CollisionAnimationLoop.h>
#include <sofa/config.h>
#include "Solver/PDBSolver.h"

namespace sofa
{

namespace component
{

namespace animationloop
{

class PBDAnimationLoop : public sofa::simulation::CollisionAnimationLoop
{
public:
    typedef typename sofa::defaulttype::Vec3dTypes DataTypes;
    typedef sofa::simulation::CollisionAnimationLoop Inherit;
    typedef typename DataTypes::VecCoord VecCoord;
    typedef typename DataTypes::VecDeriv VecDeriv;
    typedef typename DataTypes::Coord Coord;
    typedef typename DataTypes::Deriv Deriv;
    typedef typename Coord::value_type Real;
    typedef sofa::core::objectmodel::Data<VecDeriv> DataVecDeriv;
    typedef sofa::core::objectmodel::Data<VecCoord> DataVecCoord;

    SOFA_CLASS(PBDAnimationLoop, sofa::simulation::CollisionAnimationLoop);

    virtual void step (const sofa::core::ExecParams* params, SReal dt);
    virtual void init();

protected:
    PBDAnimationLoop(sofa::simulation::Node* gnode);

private:
    PDBSolver * solver;

};

} // namespace animationloop

} // namespace component

} // namespace sofa

#endif /* SOFA_COMPONENT_ANIMATIONLOOP_FREEMOTIONANIMATIONLOOP_H */
