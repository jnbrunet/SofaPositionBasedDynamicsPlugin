#include "GaussSeidelSolver.h"
#include "GaussSeidelConstraintSolver.h"

#include <sofa/core/ObjectFactory.h>

SOFA_DECL_CLASS(GaussSeidelSolver)
SOFA_DECL_CLASS(GaussSeidelConstraintSolver)

int GaussSeidelSolverClass = sofa::core::RegisterObject("Position based dynamics Gauss Seidel's solver")
        .add< GaussSeidelSolver >()
        ;
int GaussSeidelConstraintSolverClass = sofa::core::RegisterObject("Position based dynamics Gauss Seidel's constraint solver to be used with FreeMotionAnimationLoop")
        .add< sofa::core::behavior::GaussSeidelConstraintSolver >()
        ;
