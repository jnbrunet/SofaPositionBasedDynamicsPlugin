#ifndef GAUSSSEIDELSOLVER_H
#define GAUSSSEIDELSOLVER_H

#include "PDBSolver.h"
#include "../Constraints/PDBConstraint.h"

class GaussSeidelSolver : public virtual PDBSolver {
 public:
  SOFA_CLASS(GaussSeidelSolver, PDBSolver);

  typedef PDBSolver Inherited;

  GaussSeidelSolver() :
    number_of_iterations(initData(&number_of_iterations, (unsigned int) 10, "number_of_iterations", "Maximum number of iteration for the solver.")) {}

  virtual void init() {
    constraints.clear();
    this->getContext()->get<PDBBaseConstraint, std::vector<PDBBaseConstraint*>>(&constraints, sofa::core::objectmodel::BaseContext::SearchDown);
  }

  virtual bool SolveConstraints(const sofa::core::MechanicalParams* mparams, sofa::core::MultiVecCoordId x, sofa::core::MultiVecDerivId dx) {
    for (unsigned int i = 0; i < number_of_iterations.getValue(); ++i) {
      for (PDBBaseConstraint * constraint : constraints) {
        constraint->ComputeCorrection(x, dx, i);
      }
    }

    return true;
  }

  sofa::core::objectmodel::Data<unsigned int> number_of_iterations;

private:

  std::vector<PDBBaseConstraint*> constraints;
};


#endif //GAUSSSEIDELSOLVER_H
