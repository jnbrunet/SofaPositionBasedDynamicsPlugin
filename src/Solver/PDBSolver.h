#ifndef PDBSOLVER_H
#define PDBSOLVER_H

#include <sofa/core/objectmodel/BaseObject.h>
#include <sofa/core/objectmodel/BaseContext.h>
#include <sofa/core/MultiVecId.h>
#include <sofa/core/MechanicalParams.h>

class PDBSolver : public virtual sofa::core::objectmodel::BaseObject {
 public:
  SOFA_CLASS(PDBSolver, sofa::core::objectmodel::BaseObject);

  typedef sofa::core::objectmodel::BaseObject Inherited;

  virtual bool SolveConstraints(const sofa::core::MechanicalParams* mparams, sofa::core::MultiVecCoordId x, sofa::core::MultiVecDerivId dx) = 0;

};


#endif //PDBSOLVER_H
