#ifndef SOFA_CORE_BEHAVIOR_GAUSSSEIDELCONSTRAINTSOLVER_H
#define SOFA_CORE_BEHAVIOR_GAUSSSEIDELCONSTRAINTSOLVER_H
#include <sofa/core/behavior/ConstraintSolver.h>
#include <sofa/simulation/VectorOperations.h>
#include <sofa/helper/AdvancedTimer.h>

#include "../Constraints/PDBConstraint.h"

namespace sofa
{

namespace core
{

namespace behavior
{

class GaussSeidelConstraintSolver : public virtual ConstraintSolver {
public:

  GaussSeidelConstraintSolver() :
    number_of_iterations(initData(&number_of_iterations, (unsigned int) 10, "number_of_iterations", "Maximum number of iteration for the solver.")) {

  }

  virtual void init() {
    constraints.clear();
    this->getContext()->get<PDBBaseConstraint, std::vector<PDBBaseConstraint*>>(&constraints, sofa::core::objectmodel::BaseContext::SearchDown);
  }

  virtual bool prepareStates(const ConstraintParams *cParams, MultiVecId pos, MultiVecId vel=MultiVecId::null()) {
    simulation::common::VectorOperations vop(cParams, this->getContext());
    vop.v_clear(this->m_dxId);
    return true;
  }

  virtual bool buildSystem(const ConstraintParams *, MultiVecId pos, MultiVecId vel=MultiVecId::null()) {
    return true;
  }

  virtual bool solveSystem(const ConstraintParams *cParams, MultiVecId pos, MultiVecId vel=MultiVecId::null()) {
    simulation::common::VectorOperations vop(cParams, this->getContext());


    MultiVecDerivId dx(this->m_dxId);

    // Position
    if (cParams->constOrder() == ConstraintParams::ConstOrder::POS || cParams->constOrder() == ConstraintParams::ConstOrder::POS_AND_VEL) {
      MultiVecCoordId x(pos);
      ConstMultiVecCoordId xfree(cParams->x());
      vop.v_eq(x, xfree);

      sofa::helper::AdvancedTimer::stepBegin("SolvingConstraintsPDB");
      for (unsigned int i = 0; i < number_of_iterations.getValue(); ++i) {
        for (PDBBaseConstraint * constraint : constraints) {
          constraint->ComputeCorrection(x, dx, i);
        }
      }
      sofa::helper::AdvancedTimer::stepEnd("SolvingConstraintsPDB");
    }

    // Velocity
    if (cParams->constOrder() == ConstraintParams::ConstOrder::VEL || cParams->constOrder() == ConstraintParams::ConstOrder::POS_AND_VEL) {
      vop.v_eq(vel, dx, 1./0.05);
    }

    return true;
  }

  virtual bool applyCorrection(const ConstraintParams *, MultiVecId pos, MultiVecId vel=MultiVecId::null()) {
    return true;
  }

  virtual void removeConstraintCorrection(BaseConstraintCorrection *s) {

  }

  sofa::core::objectmodel::Data<unsigned int> number_of_iterations;
private:
  std::vector<PDBBaseConstraint*> constraints;
};

} // namespace behavior

} // namespace core

} // namespace sofa

#endif
