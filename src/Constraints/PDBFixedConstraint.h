#ifndef PDBFIXEDCONSTRAINT_H
#define PDBFIXEDCONSTRAINT_H

#include "PDBConstraint.h"
#include <SofaBaseTopology/TopologySubsetData.h>

template<class DataTypes>
class PDBFixedConstraint : public virtual PDBConstraint<DataTypes> {
 public:
  SOFA_CLASS(SOFA_TEMPLATE(PDBFixedConstraint, DataTypes), SOFA_TEMPLATE(PDBConstraint, DataTypes));

  typedef PDBConstraint<DataTypes> Inherited;
  typedef typename DataTypes::Coord Coord;
  typedef typename DataTypes::Deriv Deriv;
  typedef typename Coord::value_type Real;
  typedef typename DataTypes::VecCoord VecCoord;
  typedef typename DataTypes::VecDeriv VecDeriv;
  typedef sofa::core::objectmodel::Data<VecCoord>    DataVecCoord;
  typedef sofa::core::objectmodel::Data<VecDeriv>    DataVecDeriv;
  typedef sofa::helper::vector<unsigned int> SetIndexArray;
  typedef sofa::component::topology::PointSubsetData< SetIndexArray > SetIndex;

  PDBFixedConstraint() :
    Inherited(),
    f_indices( initData(&f_indices,"indices","Indices of the fixed points") ) {

  }

  virtual void init() {
    Inherited::init();
    if (!this->mstate)
       serr << "A mechanical state is required." << sendl;
    else if (f_indices.getValue().empty())
      serr<<"No fixed constraint indices was given to the component. Use the 'indices' attribute to specify which point should be fixed."<<sendl;
    else {
      const SetIndexArray & m_indices = f_indices.getValue();
      for (unsigned int i=0; i<m_indices.size(); ++i) {
        if (m_indices[i] < this->mstate->getSize())
          indices.push_back(m_indices[i]);
        else
          serr<<"Bad indice was given to the fixed constraint component (indice '"
              << std::to_string(m_indices[i])
              << "'). Make sure the indices relates to the position of the mechanical state '"
              <<this->mstate->getPathName()
              <<"' which contains only "
              <<std::to_string(this->mstate->getSize())
              <<" points."
              <<sendl;
      }
    }
  }

  virtual bool ComputeCorrection(DataVecCoord& x, DataVecDeriv& dx, unsigned int iteration_number = 0) {
    const SetIndexArray & indices = f_indices.getValue();
    sofa::helper::ReadAccessor<DataVecCoord> r0 = this->mstate->readRestPositions();
    sofa::helper::WriteAccessor<DataVecCoord> r = x;
    sofa::helper::WriteAccessor<DataVecDeriv> corrections = dx;
    for (unsigned int i=0; i<indices.size(); ++i) {
      unsigned int indice = indices[i];
      Coord correction = r0[indice] - r[indice];
      corrections[indice] += correction;
      r[indice]  += correction;
    }
    return true;
  }

 protected:
  SetIndex f_indices;

private:
  SetIndexArray indices;

};


#endif //PDBFIXEDCONSTRAINT_H
