#ifndef PDBCONSTRAINT_H
#define PDBCONSTRAINT_H

#include <sofa/core/objectmodel/BaseObject.h>
#include <sofa/core/behavior/MechanicalState.h>
#include <sofa/core/MechanicalParams.h>
#include <sofa/core/behavior/MultiVec.h>
#include <sofa/core/behavior/BaseMass.h>

class PDBBaseConstraint : public virtual sofa::core::objectmodel::BaseObject {
public:
  virtual bool ComputeCorrection(sofa::core::MultiVecCoordId x, sofa::core::MultiVecDerivId dx, unsigned int iteration_number = 0) = 0;
};

template<class DataTypes>
class PDBConstraint : public virtual PDBBaseConstraint {
 public:
  SOFA_CLASS(SOFA_TEMPLATE(PDBConstraint, DataTypes), PDBBaseConstraint);

  typedef sofa::core::objectmodel::BaseObject Inherited;
  typedef typename DataTypes::Coord Coord;
  typedef typename DataTypes::Deriv Deriv;
  typedef typename Coord::value_type Real;
  typedef typename DataTypes::VecCoord VecCoord;
  typedef typename DataTypes::VecDeriv VecDeriv;
  typedef sofa::core::objectmodel::Data<VecCoord>    DataVecCoord;
  typedef sofa::core::objectmodel::Data<VecDeriv>    DataVecDeriv;

  PDBConstraint() : stiffness(initData(&stiffness, (Real) 0.5, "K", "Stiffness factor K between 0 and 1")) {

  }

  virtual void init() {
    this->mstate = dynamic_cast<sofa::core::behavior::MechanicalState<DataTypes>*>(this->getContext()->getMechanicalState());
    if (!this->mstate)
      serr << "A mechanical state is required." << sendl;

    this->mass = this->getContext()->getMass();
    if (!this->mass)
      serr << "A mass component is required." << sendl;

    if (stiffness.getValue() > 1.0) {
      serr << "The stiffness parameter K must be between 0 and 1." << sendl;
      stiffness.setValue(0.5);
    }
  }

//  // Constraint's value
//  virtual Real C(const DataVecCoord& x) const = 0;

//  // Constraint's gradient
//  virtual std::vector<Deriv> GradC() const = 0;

//  // Constraint's gradient's squared norm
//  virtual Real GradCNorm() const = 0;

//  // Constrained indices
//  virtual std::vector<unsigned int> & Indices() const = 0;

  virtual bool ComputeCorrection(sofa::core::MultiVecCoordId x, sofa::core::MultiVecDerivId dx, unsigned int iteration_number = 0) {
    if (this->mstate)
      return ComputeCorrection(*x[this->mstate].write(), *dx[this->mstate].write());
    else
      serr << "Cannot compute the correction without a mechanical state." << sendl;

    return false;
  }

  virtual bool ComputeCorrection(DataVecCoord& x, DataVecDeriv& dx, unsigned int iteration_number = 0) = 0;

  virtual Real getK(unsigned int iteration_number) const {
    return 1. - pow(1. - stiffness.getValue(), 1./(iteration_number+1));
  }

  virtual std::string getTemplateName() const {
    return templateName(this);
  }

  static std::string templateName(const PDBConstraint<DataTypes>* = NULL) {
    return DataTypes::Name();
  }

 protected:
  sofa::core::behavior::MechanicalState<DataTypes> *mstate;
  sofa::core::behavior::BaseMass *mass;
  sofa::core::objectmodel::Data<Real> stiffness;
};


#endif //PDBCONSTRAINT_H
