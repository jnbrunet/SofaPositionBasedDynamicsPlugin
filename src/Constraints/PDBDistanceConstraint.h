#ifndef PDBDISTANCECONSTRAINT_H
#define PDBDISTANCECONSTRAINT_H

#include "PDBConstraint.h"
#include <SofaBaseTopology/EdgeSetTopologyContainer.h>
#include <SofaBaseTopology/EdgeSetGeometryAlgorithms.h>

template<class DataTypes>
class PDBDistanceConstraint : public virtual PDBConstraint<DataTypes> {
 public:
  SOFA_CLASS(SOFA_TEMPLATE(PDBDistanceConstraint, DataTypes), SOFA_TEMPLATE(PDBConstraint, DataTypes));

  typedef PDBConstraint<DataTypes> Inherited;
  typedef typename DataTypes::Coord Coord;
  typedef typename DataTypes::Deriv Deriv;
  typedef typename Coord::value_type Real;
  typedef typename DataTypes::VecCoord VecCoord;
  typedef typename DataTypes::VecDeriv VecDeriv;
  typedef sofa::core::objectmodel::Data<VecCoord>    DataVecCoord;
  typedef sofa::core::objectmodel::Data<VecDeriv>    DataVecDeriv;

  PDBDistanceConstraint() :
    Inherited(),
    edgeTopologyContainerLink(initLink("edgeTopologyContainer", "Edge topology container's component.")),
    edgeGeometryAlgoLink(initLink("edgeGeometryAlgo", "Edge topology algorithms's component.")) {

  }

  virtual void init() {
    Inherited::init();

    // Get the edge topology container
    if (edgeTopologyContainerLink.empty())
      edgeTopologyContainerLink.set(this->getContext()->template get<sofa::component::topology::EdgeSetTopologyContainer>());

    if (edgeTopologyContainerLink.empty()) {
      serr << "No edge topology container found."<<sendl;
    } else {
      edgeTopologyContainer = edgeTopologyContainerLink.get();
      std::cout<< "Edge Topo = "<<edgeTopologyContainer->getPathName()<<std::endl;
    }


    // Get the edge geometry algorithms
    if (edgeGeometryAlgoLink.empty())
      edgeGeometryAlgoLink.set(this->getContext()->template get<sofa::component::topology::EdgeSetGeometryAlgorithms<DataTypes>>());

    if (edgeGeometryAlgoLink.empty()) {
      serr << "No edge topology algorithms found."<<sendl;
    } else {
      edgeGeometryAlgo = edgeGeometryAlgoLink.get();
      std::cout<< "Edge Algo = "<<edgeGeometryAlgo->getPathName()<<std::endl;
    }
  }

  virtual bool ComputeCorrection(DataVecCoord& x, DataVecDeriv& dx, unsigned int iteration_number = 0) {
    sofa::helper::WriteAccessor<DataVecCoord> r = x;
    sofa::helper::WriteAccessor<DataVecDeriv> corrections = dx;
    for (unsigned int i = 0; i < edgeTopologyContainer->getNumberOfEdges();++i) {
      unsigned int v1 = edgeTopologyContainer->getEdge(i)[0],
                   v2 = edgeTopologyContainer->getEdge(i)[1];
      Real w1 = this->mass->getElementMass(v1), w2 = this->mass->getElementMass(v1);
      Real d = (r[v1]-r[v2]).norm();
      Real d0 = edgeGeometryAlgo->computeRestEdgeLength(i);
      Deriv n = (r[v1]-r[v2])/d;
      Real K = this->getK(iteration_number);

      Deriv dx1 = - w1 / (w1 + w2) * K * (d - d0) * n;
      Deriv dx2 = + w2 / (w1 + w2) * K * (d - d0) * n;

      corrections[v1] += dx1;
      corrections[v2] += dx2;
      r[v1] += dx1;
      r[v2] += dx2;
    }


    return true;
  }



 protected:
  sofa::core::objectmodel::SingleLink<PDBDistanceConstraint<DataTypes>, sofa::component::topology::EdgeSetTopologyContainer, sofa::core::objectmodel::BaseLink::FLAG_STRONGLINK> edgeTopologyContainerLink;
  sofa::core::objectmodel::SingleLink<PDBDistanceConstraint<DataTypes>, sofa::component::topology::EdgeSetGeometryAlgorithms<DataTypes>, sofa::core::objectmodel::BaseLink::FLAG_STRONGLINK> edgeGeometryAlgoLink;

private:
  sofa::component::topology::EdgeSetTopologyContainer * edgeTopologyContainer;
  sofa::component::topology::EdgeSetGeometryAlgorithms<DataTypes> * edgeGeometryAlgo;

};


#endif //PDBDISTANCECONSTRAINT_H
