#include "PDBDistanceConstraint.h"
#include "PDBFixedConstraint.h"
#include <sofa/core/ObjectFactory.h>
#include <SofaBaseTopology/TopologySubsetData.inl>

SOFA_DECL_CLASS(PDBDistanceConstraint)
SOFA_DECL_CLASS(PDBFixedConstraint)

int PDBDistanceConstraintClass = sofa::core::RegisterObject("Position based dynamics distance constraints")
        .add< PDBDistanceConstraint<sofa::defaulttype::Vec3dTypes> >()
        ;
int PDBFixedConstraintClass = sofa::core::RegisterObject("Position based dynamics fixed constraints")
    .add< PDBFixedConstraint<sofa::defaulttype::Vec3dTypes> >()
    ;
