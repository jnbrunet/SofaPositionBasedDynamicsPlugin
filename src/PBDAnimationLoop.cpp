#include "PBDAnimationLoop.h"

#include <sofa/core/ObjectFactory.h>
#include <sofa/simulation/UpdateContextVisitor.h>
#include <sofa/simulation/AnimateVisitor.h>
#include <sofa/simulation/AnimateBeginEvent.h>
#include <sofa/simulation/AnimateEndEvent.h>
#include <sofa/simulation/PropagateEventVisitor.h>
#include <sofa/core/VecId.h>
#include <sofa/simulation/VectorOperations.h>
#include <sofa/simulation/MechanicalOperations.h>
#include <sofa/simulation/SolveVisitor.h>
#include <sofa/core/behavior/MultiVec.h>
#include <sofa/simulation/BehaviorUpdatePositionVisitor.h>

namespace sofa
{

namespace component
{

namespace animationloop
{

PBDAnimationLoop::PBDAnimationLoop(sofa::simulation::Node *gnode) : Inherit(gnode) {

}

void PBDAnimationLoop::init() {
  {
  simulation::common::VectorOperations vop(core::ExecParams::defaultInstance(), this->getContext());
  sofa::core::behavior::MultiVecDeriv dx(&vop, core::VecDerivId::dx() ); dx.realloc( &vop, true, true );
  sofa::core::behavior::MultiVecDeriv df(&vop, core::VecDerivId::dforce() ); df.realloc( &vop, true, true );
  sofa::core::behavior::MultiVecCoord freePos(&vop, core::VecCoordId::freePosition() ); freePos.realloc( &vop, true, true );
  sofa::core::behavior::MultiVecDeriv freeVel(&vop, core::VecDerivId::freeVelocity() ); freeVel.realloc( &vop, true, true );
  }


  this->getContext()->get(solver, core::objectmodel::BaseContext::SearchDown);
  if (!solver)
    serr << "No PDBSolver found." <<sendl;
}

void PBDAnimationLoop::step(const sofa::core::ExecParams *params, SReal dt) {
  double startTime = this->gnode->getTime();
  if (dt == 0)
      dt = this->gnode->getDt();

  {
  sofa::simulation::AnimateBeginEvent ev ( dt );
  sofa::simulation::PropagateEventVisitor act ( params, &ev );
  gnode->execute ( act );
  }


  // STEP STARTS HERE
  sofa::simulation::BehaviorUpdatePositionVisitor beh(params , dt);
  simulation::common::VectorOperations vop(params, this->getContext());
  simulation::common::MechanicalOperations mop(params, this->getContext());

  sofa::core::behavior::MultiVecCoord pos(&vop, core::VecCoordId::position() );
  sofa::core::behavior::MultiVecCoord freePos(&vop, core::VecCoordId::freePosition() ); freePos.realloc( &vop, true, true );

  sofa::core::behavior::MultiVecDeriv vel(&vop, core::VecDerivId::velocity() );
  sofa::core::behavior::MultiVecDeriv freeVel(&vop, core::VecDerivId::freeVelocity() ); freeVel.realloc( &vop, true, true );


  sofa::core::behavior::MultiVecDeriv dx(&vop, core::VecDerivId::dx() ); dx.realloc( &vop, true, true );


  this->gnode->execute(&beh);


  simulation::MechanicalBeginIntegrationVisitor beginVisitor(params, dt);
  this->gnode->execute(&beginVisitor);

  sofa::core::behavior::MultiVecDeriv f  (&vop, core::VecDerivId::force() );
//  mop.addSeparateGravity(dt);


  mop.computeForce(f);
  mop.accFromF(dx, f); // dx = M^{-1} F
  freeVel.eq(vel, dx, dt); // vfree = v + dt dx
  freePos.eq(pos, freeVel, dt); // pfree = p + dt v

  // contraintes
  dx.eq(dx, 0); // Clear dx
  solver->SolveConstraints(&mop.mparams, freePos, dx);
  // fin contraintes

  //pos.eq(freePos, pos, -1); // dx = pfree - p
  //vel.eq(pos, 1.0/dt); // v = dx / dt
  vel.eq(dx,1.0/dt);
  pos.eq(freePos); // x = xfree


//  // Free Motion
//  simulation::SolveVisitor freeMotion(params, dt, true);
//  this->gnode->execute(&freeMotion); // Call ODE Solver

//  mop.propagateXAndV(freePos, freeVel, true); // apply projective constraints

//  sofa::core::behavior::MultiVecDeriv dx(&vop, core::VecDerivId::dx() ); dx.realloc( &vop, true, true );
//  sofa::core::behavior::MultiVecDeriv dv(&vop, core::VecDerivId::dx() ); dv.realloc( &vop, true, true );
//  simulation::MechanicalVOpVisitor(params, pos, freePos, dx, 1.0 ).setOnlyMapped(false).execute(this->gnode);
//  simulation::MechanicalVOpVisitor(params, vel, freeVel, dv, 1.0 ).setOnlyMapped(false).execute(this->gnode);

  // STEP STOPS HERE

  this->gnode->setTime ( startTime + dt );
  this->gnode->execute<sofa::simulation::UpdateSimulationContextVisitor>(params);  // propagate time

  {
  sofa::simulation::AnimateEndEvent ev ( dt );
  sofa::simulation::PropagateEventVisitor act ( params, &ev );
  gnode->execute ( act );
  }
}

} // namespace animationloop

} // namespace component

} // namespace sofa

SOFA_DECL_CLASS(PBDAnimationLoop)

int PBDAnimationLoopClass = sofa::core::RegisterObject("Position based dynamics animation loop")
        .add< sofa::component::animationloop::PBDAnimationLoop >()
        ;
