#include <sofa/helper/system/config.h>

namespace sofa {

namespace component {

extern "C" {
void initExternalModule();
const char* getModuleName();
const char* getModuleVersion();
const char* getModuleLicense();
const char* getModuleDescription();
const char* getModuleComponentList();
}

void initExternalModule()
{
  static bool first = true;
  if (first)
  {
    first = false;
  }
}

const char* getModuleName()
{
  return "Plugin PBD";
}

const char* getModuleVersion()
{
  return "alpha 1.0";
}

const char* getModuleLicense()
{
  return "LGPL";
}

const char* getModuleDescription()
{
  return "Position based dynamics plugin for SOFA Framework";
}

const char* getModuleComponentList()
{
  return "animationloop";
}

}

}

SOFA_LINK_CLASS(PBDAnimationLoop)
SOFA_LINK_CLASS(PDBFixedConstraint)
SOFA_LINK_CLASS(PDBDistanceConstraint)
SOFA_LINK_CLASS(GaussSeidelSolver)
